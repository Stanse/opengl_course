#ifndef ENGINE_HPP
#define ENGINE_HPP
#include "glad/glad.h"
#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

#include <SDL.h>

struct vertex {
  vertex() : x(0.f), y(0.f) {}
  float x;
  float y;
  float z;
};

struct triangle {
  triangle() {
    v[0] = vertex();
    v[1] = vertex();
    v[2] = vertex();
  }
  vertex v[3];
};

enum class EngineEvent {
  /// input events
  left_pressed,
  left_released,
  right_pressed,
  right_released,
  up_pressed,
  up_released,
  down_pressed,
  down_released,
  select_pressed,
  select_released,
  start_pressed,
  start_released,
  button1_pressed,
  button1_released,
  button2_pressed,
  button2_released,
  /// virtual console events
  turn_off
};

std::ostream &operator<<(std::ostream &stream, const EngineEvent event);

std::istream &operator>>(std::istream &is, vertex &v);
std::istream &operator>>(std::istream &is, triangle &t);

class Engine {
public:
  Engine(int, int);
  ~Engine();
  int screen_width, screen_height;
  int init();
  bool read_input(EngineEvent &);
  int load();
  int quit();
  void clear();
  void swap_buffers();
  void render_triangle(const triangle &);

  void create_vertex_shader();
  void create_fragment_shader();
  void create_shader_prog();

  void create_my_vertex_shader();
  void create_my_fragment_shader();
  void create_my_shader_prog();
  void render_my_triangle();

  void bind_atribute(unsigned int);

  void validate_opengl_errors();
  void print_opengl_version();

private:
  SDL_Window *window = nullptr;
  SDL_GLContext gl_context = nullptr;
  GLuint program_id_ = 0;
  GLuint vertex_shader = 0;
  GLuint fragment_shader = 0;

  GLuint my_shader_prog = 0;
  GLuint my_vertex_shader = 0;
  GLuint my_fragment_shader = 0;
};

#endif // ENGINE_HPP
