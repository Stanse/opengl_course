#include "engine.hpp"

#define CHECK_OPENGL()                                                         \
  {                                                                            \
    const int err = static_cast<int>(glGetError());                            \
    if (err != GL_NO_ERROR) {                                                  \
      switch (err) {                                                           \
      case GL_INVALID_ENUM:                                                    \
        std::cerr << GL_INVALID_ENUM << std::endl;                             \
        break;                                                                 \
      case GL_INVALID_VALUE:                                                   \
        std::cerr << GL_INVALID_VALUE << std::endl;                            \
        break;                                                                 \
      case GL_INVALID_OPERATION:                                               \
        std::cerr << GL_INVALID_OPERATION << std::endl;                        \
        break;                                                                 \
      case GL_INVALID_FRAMEBUFFER_OPERATION:                                   \
        std::cerr << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl;            \
        break;                                                                 \
      case GL_OUT_OF_MEMORY:                                                   \
        std::cerr << GL_OUT_OF_MEMORY << std::endl;                            \
        break;                                                                 \
      }                                                                        \
      assert(false);                                                           \
    }                                                                          \
  }

void Engine::validate_opengl_errors()
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        std::string message;
        switch (error)
        {
        case GL_INVALID_ENUM:
            message = "invalid enum passed to GL function (GL_INVALID_ENUM)";
            break;
        case GL_INVALID_VALUE:
            message = "invalid parameter passed to GL function (GL_INVALID_VALUE)";
            break;
        case GL_INVALID_OPERATION:
            message = "cannot execute some of GL functions in current state (GL_INVALID_OPERATION)";
            break;
        case GL_OUT_OF_MEMORY:
            message = "no enough memory to execute GL function (GL_OUT_OF_MEMORY)";
            break;
        default:
            message = "error in some GL extension (framebuffers, shaders, etc)";
            break;
        }
        std::cerr << "OpenGL error: " << message << std::endl;
        std::abort();
    }
}

// Set up vertex data (and buffer(s)) and attribute pointers
   GLfloat vertices[] = {
       // Первый треугольник
            0.5f,  0.5f, 0.0f,  // Верхний правый угол
            0.5f, -0.5f, 0.0f,  // Нижний правый угол
           -0.5f,  0.5f, 0.0f,  // Верхний левый угол
           // Второй треугольник
            0.5f, -0.5f, 0.0f,  // Нижний правый угол
           -0.5f, -0.5f, 0.0f,  // Нижний левый угол
           -0.5f,  0.5f, 0.0f   // Верхний левый угол

   };

   GLuint indices[] = {
       0, 1, 3,   // Первый треугольник
       1, 2, 3    // Второй треугольник
   };

static std::array<std::string, 17> event_names = {
    {/// input events
     "left_pressed", "left_released", "right_pressed", "right_released",
     "up_pressed", "up_released", "down_pressed", "down_released",
     "select_pressed", "select_released", "start_pressed", "start_released",
     "button1_pressed", "button1_released", "button2_pressed",
     "button2_released",
     /// virtual console events
     "turn_off"}};

std::ostream &operator<<(std::ostream &stream, const EngineEvent event) {
  std::uint32_t value = static_cast<std::uint32_t>(event);
  std::uint32_t minimal = static_cast<std::uint32_t>(EngineEvent::left_pressed);
  std::uint32_t maximal = static_cast<std::uint32_t>(EngineEvent::turn_off);
  if (value >= minimal && value <= maximal) {
    stream << event_names[value];
    return stream;
  } else {
    throw std::runtime_error("too big event value");
  }
}

static std::ostream &operator<<(std::ostream &out, const SDL_version &v) {
  out << static_cast<int>(v.major) << '.';
  out << static_cast<int>(v.minor) << '.';
  out << static_cast<int>(v.patch);
  return out;
}

std::istream &operator>>(std::istream &is, vertex &v) {
  is >> v.x;
  is >> v.y;
  is >> v.z;
  return is;
}

std::istream &operator>>(std::istream &is, triangle &t) {
  is >> t.v[0];
  is >> t.v[1];
  is >> t.v[2];
  return is;
}

struct EngineBind {
  SDL_Keycode key;
  std::string_view name;
  EngineEvent event_pressed;
  EngineEvent event_released;
};

const std::array<EngineBind, 8> keys{
    {{SDLK_w, "up", EngineEvent::up_pressed, EngineEvent::up_released},
     {SDLK_a, "left", EngineEvent::left_pressed, EngineEvent::left_released},
     {SDLK_s, "down", EngineEvent::down_pressed, EngineEvent::down_released},
     {SDLK_d, "right", EngineEvent::right_pressed, EngineEvent::right_released},
     {SDLK_LCTRL, "button1", EngineEvent::button1_pressed,
      EngineEvent::button1_released},
     {SDLK_SPACE, "button2", EngineEvent::button2_pressed,
      EngineEvent::button2_released},
     {SDLK_ESCAPE, "select", EngineEvent::select_pressed,
      EngineEvent::select_released},
     {SDLK_RETURN, "start", EngineEvent::start_pressed,
      EngineEvent::start_released}}};

static bool check_input(const SDL_Event &e, const EngineBind *&result) {
  using namespace std;

  const auto it = find_if(begin(keys), end(keys), [&](const EngineBind &b) {
    return b.key == e.key.keysym.sym;
  });

  if (it != end(keys)) {
    result = &(*it);
    return true;
  }
  return false;
}

Engine::Engine(int w, int h) {
  screen_width = w;
  screen_height = h;

};

int Engine::init() {
  using namespace std;
  stringstream serr;

  SDL_version compiled = {0, 0, 0};
  SDL_version linked = {0, 0, 0};
  SDL_VERSION(&compiled)
  SDL_GetVersion(&linked);
  if (SDL_COMPILEDVERSION !=
      SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
    cout << "warning: SDL2 compiled and linked version mismatch: " << &compiled
         << " " << &linked << endl;
    return 1;
  }

  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    cout << "error: can't init: " << SDL_GetError() << endl;
    system("pause");
    return 1;
  }

  window = SDL_CreateWindow("OpenGL_1", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, screen_width,
                            screen_height, ::SDL_WINDOW_OPENGL);
  if (window == nullptr) {
    std::cout << "error: can't create window: " << SDL_GetError() << std::endl;
    system("pause");
    return 1;
  }

  int gl_major_ver = 2;
  int gl_minor_ver = 0;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  if (gl_context == nullptr) {
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    gl_context = SDL_GL_CreateContext(window);
  }

  assert(gl_context != nullptr);

  int result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
  assert(result == 0);

  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
  assert(result == 0);

  if (gl_major_ver < 2) {
    std::clog << "current context opengl version: " << gl_major_ver << '.'
              << gl_minor_ver << '\n'
              << "need openg version at least: 2.1\n"
              << std::flush;
    throw std::runtime_error("opengl version too low");
  }

  if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) {
    std::clog << "error: failed to initialize glad" << std::endl;
  }


      create_shader_prog();
      bind_atribute(program_id_);

//    create_my_shader_prog();
//    bind_atribute(my_shader_prog);

//    glDeleteShader(my_vertex_shader);
//    glDeleteShader(my_fragment_shader);

  return 0;
}

void Engine::create_vertex_shader(){
    using namespace std;
    stringstream serr;

    // create vertex shader
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    CHECK_OPENGL()
    string_view vertex_shader_src = R"(
                                    attribute vec3 a_position;
                                    varying vec4 v_position;

                                    void main()
                                    {
                                        v_position = vec4(a_position, 1.0);
                                        gl_Position = v_position;
                                    }
                                    )";
    const char *source = vertex_shader_src.data();
    glShaderSource(vertex_shader, 1, &source, nullptr);
    CHECK_OPENGL()

    glCompileShader(vertex_shader);
    CHECK_OPENGL()

    GLint compiled_status = 0;
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compiled_status);
    CHECK_OPENGL()
    if (compiled_status == 0) {
      GLint info_len = 0;
      glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &info_len);
      CHECK_OPENGL()
      std::vector<char> info_chars(static_cast<size_t>(info_len));
      glGetShaderInfoLog(vertex_shader, info_len, nullptr, info_chars.data());
      CHECK_OPENGL()
      glDeleteShader(vertex_shader);
      CHECK_OPENGL()

      std::string shader_type_name = "vertex";
      serr << "error: compiling shader(vertex)\n"
           << vertex_shader_src << "\n"
           << info_chars.data();
      cout << serr.str() << endl;
      //return 1;
    }
};

void Engine::create_fragment_shader(){
    using namespace std;
    stringstream serr;

    // create fragment shader
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    CHECK_OPENGL()
    string_view fragment_shader_src = R"(
                      precision mediump float;
                      varying vec4 v_position;

                      void main()
                      {
                          gl_FragColor = vec4(1.0, .5, 0.2, 1.0);
                      }
                      )";
    const char *source = fragment_shader_src.data();
    glShaderSource(fragment_shader, 1, &source, nullptr);
    CHECK_OPENGL()

    glCompileShader(fragment_shader);
    CHECK_OPENGL()

    GLint compiled_status = 0;
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled_status);
    CHECK_OPENGL()
    if (compiled_status == 0) {
      GLint info_len = 0;
      glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &info_len);
      CHECK_OPENGL()
      std::vector<char> info_chars(static_cast<size_t>(info_len));
      glGetShaderInfoLog(fragment_shader, info_len, nullptr, info_chars.data());
      CHECK_OPENGL()
      glDeleteShader(fragment_shader);
      CHECK_OPENGL()

      serr << "error: compiling shader(fragment)\n"
           << fragment_shader_src << "\n"
           << info_chars.data();
      cout << serr.str() << endl;
      //return 1;
    }
};

void Engine::create_shader_prog(){
    using namespace std;
    stringstream serr;

    create_vertex_shader();
    create_fragment_shader();

    // now create program and attach vertex and fragment shaders
    program_id_ = glCreateProgram();
    CHECK_OPENGL()
    if (0 == program_id_) {
      serr << "error: failed to create gl program";
      //return 1;
    }

    glAttachShader(program_id_, vertex_shader);
    CHECK_OPENGL()
    glAttachShader(program_id_, fragment_shader);
    CHECK_OPENGL()
};

void Engine::render_triangle(const triangle &t) {
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), &t.v[0]);
  CHECK_OPENGL()
  glEnableVertexAttribArray(0);
  CHECK_OPENGL()
  glValidateProgram(program_id_);
  CHECK_OPENGL()
  // Check the validate status
  GLint validate_status = 0;
  glGetProgramiv(program_id_, GL_VALIDATE_STATUS, &validate_status);
  CHECK_OPENGL()
  if (validate_status == GL_FALSE) {
    GLint infoLen = 0;
    glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
    CHECK_OPENGL()
    std::vector<char> infoLog(static_cast<size_t>(infoLen));
    glGetProgramInfoLog(program_id_, infoLen, nullptr, infoLog.data());
    CHECK_OPENGL()
    std::cerr << "Error linking program:\n" << infoLog.data();
    throw std::runtime_error("error");
  }
  //---------------------------
  glDrawArrays(GL_TRIANGLES, 0, 3);
  CHECK_OPENGL()
}

//***************** My SHADERS *****************

void Engine::create_my_vertex_shader(){
    using namespace std;
    stringstream serr;

    // create vertex shader
    my_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    // биндим a_position, но оно работает?
    CHECK_OPENGL()
    string_view my_vertex_shader_src = R"(
                                    #version 320 es
                                    layout (location = 0) in vec3 position;

                                    void main()
                                    {
                                        gl_Position = vec4(position, 1.0);
                                    }
                                    )";
    const char *source = my_vertex_shader_src.data();
    glShaderSource(my_vertex_shader, 1, &source, nullptr);
    CHECK_OPENGL()

    glCompileShader(my_vertex_shader);
    CHECK_OPENGL()

    GLint compiled_status = 0;
    glGetShaderiv(my_vertex_shader, GL_COMPILE_STATUS, &compiled_status);
    CHECK_OPENGL()
    if (compiled_status == 0) {
      GLint info_len = 0;
      glGetShaderiv(my_vertex_shader, GL_INFO_LOG_LENGTH, &info_len);
      CHECK_OPENGL()
      std::vector<char> info_chars(static_cast<size_t>(info_len));
      glGetShaderInfoLog(my_vertex_shader, info_len, nullptr, info_chars.data());
      CHECK_OPENGL()
      glDeleteShader(my_vertex_shader);
      CHECK_OPENGL()

      std::string shader_type_name = "vertex";
      serr << "error: compiling shader(vertex)\n"
           << my_vertex_shader_src << "\n"
           << info_chars.data();
      cout << serr.str() << endl;
      //return 1;
    }
};

void Engine::create_my_fragment_shader(){
    using namespace std;
    stringstream serr;

    // create fragment shader
    my_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    CHECK_OPENGL()
    string_view my_fragment_shader_src = R"(
                      #version 320 es
                      precision mediump float;
                      out vec4 color;

                      void main()
                      {
                          color = vec4(1.0, .5, 0.2, 1.0);
                      }
                      )";
    const char *source = my_fragment_shader_src.data();
    glShaderSource(my_fragment_shader, 1, &source, nullptr);
    CHECK_OPENGL()

    glCompileShader(my_fragment_shader);
    CHECK_OPENGL()

    GLint compiled_status = 0;
    glGetShaderiv(my_fragment_shader, GL_COMPILE_STATUS, &compiled_status);
    CHECK_OPENGL()
    if (compiled_status == 0) {
      GLint info_len = 0;
      glGetShaderiv(my_fragment_shader, GL_INFO_LOG_LENGTH, &info_len);
      CHECK_OPENGL()
      std::vector<char> info_chars(static_cast<size_t>(info_len));
      glGetShaderInfoLog(my_fragment_shader, info_len, nullptr, info_chars.data());
      CHECK_OPENGL()
      glDeleteShader(my_fragment_shader);
      CHECK_OPENGL()

      serr << "error: compiling shader(fragment)\n"
           << my_fragment_shader_src << "\n"
           << info_chars.data();
      cout << serr.str() << endl;
      //return 1;
    }
};

void Engine::create_my_shader_prog(){
    using namespace std;
    stringstream serr;

    create_my_vertex_shader();
    create_my_fragment_shader();

    // now create program and attach vertex and fragment shaders
    my_shader_prog = glCreateProgram();
    CHECK_OPENGL()
    if (0 == my_shader_prog) {
      serr << "error: failed to create gl program";
      //return 1;
    }

    glAttachShader(my_shader_prog, my_vertex_shader);
    CHECK_OPENGL()
    glAttachShader(my_shader_prog, my_fragment_shader);
    CHECK_OPENGL()
};

void Engine::bind_atribute(unsigned int shader_prog){
    using namespace std;
    stringstream serr;
    // bind attribute location
    glBindAttribLocation(shader_prog, 0, "a_position");
    CHECK_OPENGL()

    // link program after binding attribute locations
    glLinkProgram(shader_prog);
    CHECK_OPENGL()
    // Check the link status
    GLint linked_status = 0;
    glGetProgramiv(shader_prog, GL_LINK_STATUS, &linked_status);
    CHECK_OPENGL()
    if (linked_status == 0) {
      GLint infoLen = 0;
      glGetProgramiv(shader_prog, GL_INFO_LOG_LENGTH, &infoLen);
      CHECK_OPENGL()
      std::vector<char> infoLog(static_cast<size_t>(infoLen));
      glGetProgramInfoLog(shader_prog, infoLen, nullptr, infoLog.data());
      CHECK_OPENGL()
      serr << "Error linking program:\n" << infoLog.data();
      cout << serr.str() << endl;
      glDeleteProgram(shader_prog);
      CHECK_OPENGL()
      //return 1;
    }

    // turn on rendering with just created shader program
    glUseProgram(shader_prog);
    CHECK_OPENGL()

    glEnable(GL_DEPTH_TEST);

    // glDisable(GL_DEPTH_TEST);
}

void Engine::render_my_triangle(){

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), &vertices);
    CHECK_OPENGL()
    glEnableVertexAttribArray(0);
    CHECK_OPENGL()
    glValidateProgram(my_shader_prog);
    CHECK_OPENGL()
    // Check the validate status
    GLint validate_status = 0;
    glGetProgramiv(my_shader_prog, GL_VALIDATE_STATUS, &validate_status);
    CHECK_OPENGL()
    if (validate_status == GL_FALSE) {
      GLint infoLen = 0;
      glGetProgramiv(my_shader_prog, GL_INFO_LOG_LENGTH, &infoLen);
      CHECK_OPENGL()
      std::vector<char> infoLog(static_cast<size_t>(infoLen));
      glGetProgramInfoLog(my_shader_prog, infoLen, nullptr, infoLog.data());
      CHECK_OPENGL()
      std::cerr << "Error linking program:\n" << infoLog.data();
      throw std::runtime_error("error");
    }
    //---------------------------
    glDrawArrays(GL_TRIANGLES, 0, 3);
    //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    CHECK_OPENGL()
}

void Engine::swap_buffers() {
  SDL_GL_SwapWindow(window);

  glClearColor(0.3f, 0.3f, 1.0f, 0.0f);
  CHECK_OPENGL()
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  CHECK_OPENGL()
};

bool Engine::read_input(EngineEvent &e) {
  using namespace std;
  // collect all events from SDL
  SDL_Event sdl_event;
  if (SDL_PollEvent(&sdl_event)) {
    const EngineBind *binding = nullptr;

    if (sdl_event.type == SDL_QUIT) {
      e = EngineEvent::turn_off;
      return true;
    } else if (sdl_event.type == SDL_KEYDOWN) {
      if (check_input(sdl_event, binding)) {
        e = binding->event_pressed;
        return true;
      }
    } else if (sdl_event.type == SDL_KEYUP) {
      if (check_input(sdl_event, binding)) {
        e = binding->event_released;
        return true;
      }
    }
  }
  return false;
}

int Engine::quit() {
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
};

void Engine::clear() {
  glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
  CHECK_OPENGL()
  glClear(GL_COLOR_BUFFER_BIT);
  CHECK_OPENGL()
};

void Engine::print_opengl_version(){
    using namespace std;
    string version = reinterpret_cast<const char*>(glGetString(GL_VERSION));
    string vendor_info = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
    string extentions_info = reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
    cerr << "OpenGL version: " << version << endl;
    cerr << "OpenGL vendor: " << vendor_info << endl;
    cerr << "Full OpenGL extention list: " << extentions_info << endl;
};


Engine::~Engine() { this->quit(); }
