#include "engine.hpp"
#include <iostream>

int main(int /*argc*/, char ** /*argv*/) {
  Engine engine(640, 480);
  engine.init();
  engine.print_opengl_version();


  bool run{true};
  EngineEvent event;
  while (run) {

    while (engine.read_input(event)) {
      std::cout << event << std::endl;
      switch (event) {
      case EngineEvent::turn_off:
        run = false;
        break;
      default:
        break;
      }
    }

    std::ifstream file("vertexes.txt");
    assert(!!file);

    engine.clear();
    triangle tr;
    file >> tr;

    engine.render_triangle(tr);

    file >> tr;
    engine.render_triangle(tr);

//    engine.clear();
//    engine.render_my_triangle();
//    engine.render_my_triangle();

    engine.swap_buffers();
  }
  return 0;
}
