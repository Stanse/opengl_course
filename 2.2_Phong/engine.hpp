#pragma once
#include "game_object.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "shader.hpp"
#include <SDL.h>
#include <vector>

class Engine
{
public:
    Engine(int, int);
    ~Engine();

    int  init();
    void clear_display();
    void swap_buffers();
    void print_opengl_version();
    void load_texture(Game_object& game_obj, const char* path);
    void bind_game_object(Game_object& game_obj);
    void draw(Game_object& game_obj);
    void validate_opengl_errors();
    int  quit();

    void read_input(bool& run);
    void do_movement(bool& run);

    GLfloat calc_delta_time();

    float getime() { return SDL_GetTicks(); }
    void  set_screen_w(int w) { screen_width = w; }
    void  set_screen_h(int h) { screen_height = h; }
    int   get_screen_w() { return screen_width; }
    int   get_screen_h() { return screen_height; }

    Shader*                  lampShader     = nullptr;
    Shader*                  lightingShader = nullptr;
    std::vector<Game_object> game_objects;
    GLuint                   light_VBO, light_VAO;
    glm::vec3                lightPos;
    GLuint                   game_obj_VBO, game_obj_VAO;

    SDL_Event e;
    bool      keys[1024] = { false };
    GLfloat   cameraSpeed;
    GLfloat   fov;
    glm::vec3 cameraPos;
    glm::vec3 cameraFront;
    glm::vec3 cameraUp;

    bool    first_mouse = true;
    GLint   xpos, ypos;
    GLfloat lastX;
    GLfloat lastY;
    GLfloat yaw;
    GLfloat pitch;

private:
    int           screen_width, screen_height;
    SDL_Window*   window     = nullptr;
    SDL_GLContext gl_context = nullptr;
};
