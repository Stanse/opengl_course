#include "shader.hpp"

Shader::Shader(const GLchar *vertexPath, const GLchar *fragmentPath) {
  // 1. Получаем исходный код шейдера из filePath
  using namespace std;
  string vertexCode;
  string fragmentCode;
  ifstream vShaderFile;
  ifstream fShaderFile;

  // Удостоверимся, что ifstream объекты могут выкидывать исключения
  vShaderFile.exceptions(ifstream::badbit);
  fShaderFile.exceptions(ifstream::badbit);

  try {
    // Open file
    vShaderFile.open(vertexPath);
    fShaderFile.open(fragmentPath);
    stringstream vShaderStream, fShaderStream;

    // Read data to stream
    vShaderStream << vShaderFile.rdbuf();
    fShaderStream << fShaderFile.rdbuf();

    // close files
    vShaderFile.close();
    fShaderFile.close();

    // cast streams to arr GLchar
    vertexCode = vShaderStream.str();
    fragmentCode = fShaderStream.str();

  } catch (ifstream::failure e) {
    cout << "ERROR: Shader file not succesfully read." << endl;
  }

  const GLchar *vShaderCode = vertexCode.c_str();
  const GLchar *fShaderCode = fragmentCode.c_str();

  // 2. Сборка шейдеров
  GLuint vertex, fragment;
  GLint success;
  GLchar infoLog[512];

  // Vertex shader
  vertex = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex, 1, &vShaderCode, nullptr);
  glCompileShader(vertex);
  // check to error
  glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(vertex, 512, nullptr, infoLog);
    cout << "ERROR: Vertex shader compilation failed\n" << infoLog << endl;
  }

  // Fragment shader
  fragment = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment, 1, &fShaderCode, nullptr);
  glCompileShader(fragment);
  // check to error
  glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(fragment, 512, nullptr, infoLog);
    cout << "ERROR: Fragment shader compilation failed\n" << infoLog << endl;
  }

  // Create shader program
  this->Program = glCreateProgram();
  glAttachShader(this->Program, vertex);
  glAttachShader(this->Program, fragment);
  glLinkProgram(this->Program);
  // Check to error
  glGetProgramiv(this->Program, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(this->Program, 512, nullptr, infoLog);
    cout << "ERROR: Shader program linking failed\n" << infoLog << endl;
  }
  // delete shaders
  glDeleteShader(vertex);
  glDeleteShader(fragment);
}

void Shader::Use() { glUseProgram(this->Program); }
