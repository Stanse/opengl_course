cmake_minimum_required(VERSION 3.16)

project(HelloTriangle CXX C)

add_executable(HelloTriangle main.cpp glad/glad.c shader.cpp stb_image.h)
target_compile_features(HelloTriangle PUBLIC cxx_std_17)

find_package(sdl2 REQUIRED)

target_include_directories(HelloTriangle PUBLIC
    ${SDL2_INCLUDE_DIRS}
    #/usr/local/include/SDL2/
    ${CMAKE_CURRENT_LIST_DIR}
    )
target_link_libraries(HelloTriangle PUBLIC
    ${SDL2_LIBRARIES}
    #/usr/local/lib/libSDL2-2.0d.so
    )
    
target_link_libraries(HelloTriangle PRIVATE -lSDL2 -lGL)
