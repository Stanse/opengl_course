#include <iostream>
#include "gameengine.hpp"

using namespace std;

int main()
{
    asge::Game_engine* engine = asge::create_game_engine();
    engine->initialize();

    std::cout << "Hello World!" << std::endl;
    return 0;
}


