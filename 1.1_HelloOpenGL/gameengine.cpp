#include "gameengine.hpp"

#include "glad/glad.h"
#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

#include <SDL.h>

// screen paremeters:
extern int screen_width;
extern int screen_height;

namespace asge {

class Asengine : public Game_engine {
public:
  Asengine() {}
  Asengine(int w, int h) {
    screen_width = w;
    screen_height = h;
  }
  ~Asengine() {}
  int screen_width = 800;
  int screen_height = 600;
  /// create main window
  /// on success return empty string
  int initialize() final;
  /// return seconds from initialization
  float get_time_from_init() final {
    std::uint32_t ms_from_library_initialization = SDL_GetTicks();
    float seconds = ms_from_library_initialization * 0.001f;
    return seconds;
  }

private:
  SDL_Window *window = nullptr;
  SDL_GLContext gl_context = nullptr;
  GLuint program_id_ = 0;
  GLuint vertex_shader = 0;
  GLuint fragment_shader = 0;
  GLuint my_shader_prog = 0;
  GLuint my_vertex_shader = 0;
  GLuint my_fragment_shader = 0;
};

int Asengine::initialize() {
  using namespace std;
  stringstream serr;

  SDL_version compiled = {0, 0, 0};
  SDL_version linked = {0, 0, 0};
  SDL_VERSION(&compiled)
  SDL_GetVersion(&linked);
  if (SDL_COMPILEDVERSION !=
      SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
    cout << "warning: SDL2 compiled and linked version mismatch: " << &compiled
         << " " << &linked << endl;
    return 1;
  }

  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    cout << "error: can't init: " << SDL_GetError() << endl;
    system("pause");
    return 1;
  }

  window = SDL_CreateWindow("OpenGL_1", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, screen_width,
                            screen_height, ::SDL_WINDOW_OPENGL);
  if (window == nullptr) {
    std::cout << "error: can't create window: " << SDL_GetError() << std::endl;
    system("pause");
    return 1;
  }

  int gl_major_ver = 2;
  int gl_minor_ver = 0;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  if (gl_context == nullptr) {
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    gl_context = SDL_GL_CreateContext(window);
  }

  assert(gl_context != nullptr);

  int result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
  assert(result == 0);

  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
  assert(result == 0);

  if (gl_major_ver < 2) {
    std::clog << "current context opengl version: " << gl_major_ver << '.'
              << gl_minor_ver << '\n'
              << "need openg version at least: 2.1\n"
              << std::flush;
    throw std::runtime_error("opengl version too low");
  }

  if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) {
    std::clog << "error: failed to initialize glad" << std::endl;
  }

  SDL_Delay(2000);
  // create_shader_prog();
  // bind_atribute(program_id_);

  //    create_my_shader_prog();
  //    bind_atribute(my_shader_prog);

  //    glDeleteShader(my_vertex_shader);
  //    glDeleteShader(my_fragment_shader);

  return 0;
}

static bool already_exist = false;
Game_engine *create_game_engine() {
  if (already_exist) {
    throw std::runtime_error("engine already exist");
  }
  Game_engine *result = new Asengine(800, 600);
  already_exist = true;
  return result;
}

Game_engine::~Game_engine(){};
} // namespace asge
