#ifndef GAMEENGINE_HPP
#define GAMEENGINE_HPP
#include <iosfwd>
#include <string>
#include <string_view>

namespace asge {
class Game_engine
{
public:
    virtual ~Game_engine();
    /// create main window
    /// on success return empty string
    virtual int initialize() = 0;
    /// return seconds from initialization
    virtual float get_time_from_init() = 0;

    //virtual void swap_buffers() = 0;
    //virtual void uninitialize() = 0;
};

Game_engine *create_game_engine();

} // end namespace asge

#endif // GAMEENGINE_HPP
