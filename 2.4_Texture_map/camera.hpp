#pragma once

#include "glad/glad.h" // OpenGL ES 3.0
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <vector>

// Defines several possible options for camera movement.
enum Camera_Movement
{
    forward,
    backward,
    left,
    right
};

// Default camera values
const GLfloat d_yaw         = -90.0f;
const GLfloat d_pitch       = 0.0f;
const GLfloat d_speed       = 0.005f;
const GLfloat d_sensitivity = 0.9f;
const GLfloat d_zoom        = 45.0f;

// An abstract camera class that processes input and calculates the
// corresponding Euler Angles, Vectors and Matrices for use in OpenGL
class Camera
{
public:
    // Camera atributes:
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 up;
    glm::vec3 right;
    glm::vec3 world_up;

    // Euler angles
    GLfloat yaw;
    GLfloat pitch;

    // Camera options
    GLfloat movement_speed;
    GLfloat mouse_sensitivity;
    GLfloat zoom;

    // ====================
    // CONSTRUCTORS
    // ====================
    // Constructor with vectors
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
           glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = d_yaw,
           GLfloat pitch = d_pitch)
        : front(glm::vec3(0.0f, 0.0f, -1.0f))
        , movement_speed(d_speed)
        , mouse_sensitivity(d_sensitivity)
        , zoom(d_zoom)
    {
        this->position = position;
        this->world_up = up;
        this->yaw      = yaw;
        this->pitch    = pitch;
        update_camera_vectors();
    }
    // Constructor with scalar values
    Camera(float posX, float posY, float posZ, float upX, float upY, float upZ,
           float yaw, float pitch)
        : front(glm::vec3(0.0f, 0.0f, -1.0f))
        , movement_speed(d_speed)
        , mouse_sensitivity(d_sensitivity)
        , zoom(d_zoom)
    {
        position    = glm::vec3(posX, posY, posZ);
        world_up    = glm::vec3(upX, upY, upZ);
        this->yaw   = yaw;
        this->pitch = pitch;
        update_camera_vectors();
    }

    // Returns the view matrix calculated using Euler Angles and the LookAt
    // Matrix
    glm::mat4 GetViewMatrix()
    {
        return glm::lookAt(position, position + front, up);
    }

    // Calculates the front vector from the Camera's (updated) Euler Angles
    void update_camera_vectors()
    {
        glm::vec3 new_front;
        new_front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
        new_front.y = sin(glm::radians(pitch));
        new_front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
        front       = glm::normalize(new_front);

        // Also re-calculate the Right and Up vector
        right = glm::normalize(glm::cross(
            front, world_up)); // Normalize the vectors, because their length
                               // gets closer to 0 the more you look up or down
                               // which results in slower movement.
        up = glm::normalize(glm::cross(right, front));
    }
};
