#include <exception>
#include <functional>
#include <iostream>
#include <vector>

//#include <glad/glad.h> // OpenGL ES 3.0
//#include <SDL_opengles2.h>
#include "SOIL/SOIL.h"
#include "shader.hpp"
#include "stb_image.h"
#include <SDL.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int
main(int /*argc*/, char* /*args*/[])
{

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    return 1;
  }

  SDL_Surface* screen_surface = nullptr;
  SDL_Window* window = nullptr;

  window = SDL_CreateWindow("title",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED,
                            SCREEN_WIDTH,
                            SCREEN_HEIGHT,
                            ::SDL_WINDOW_OPENGL);

  if (window == nullptr) {
    return 1;
  }

  SDL_GLContext gl_context = SDL_GL_CreateContext(window);

  if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) { // Important!
    std::cout << "error: failed to initialize glad" << std::endl;
  }

  Shader simpleShader("shader.vs", "shader.frag");
  Shader simpleDebugShader("shader.vs", "shaderDebug.frag");

  // Set up vertex data (and buffer(s)) and attribute pointers
  // clang-format off
  GLfloat vertices[] = {
      // Позиции              // Цвета            // Текстурные координаты
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   2.0f, 2.0f,   // Верхний правый
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   2.0f, 0.0f,   // Нижний правый
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // Нижний левый
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 2.0f    // Верхний левый
       };
  // clang-format on
  GLuint indices[] = {
    // Note that we start from 0!
    0, 1, 3, // First Triangle
    1, 2, 3  // Second Triangle
  };

  GLuint VBO, VAO, EBO;
  // glad_glEnableVertexAttribArray(1);
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);

  // Bind the Vertex Array Object first, then bind and set vertex buffer(s) and
  // attribute pointer(s).
  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(
    GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

  // clang-format off
   // Position attribute
   glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), static_cast<GLvoid*>(nullptr));
   glEnableVertexAttribArray(0);

   // Color attribute
   glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(3 * sizeof (GLfloat)));
   glEnableVertexAttribArray(1);

   // Texture coord attribute
   glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof (GLfloat), reinterpret_cast<GLvoid*>(6 * sizeof (GLfloat)));
   glEnableVertexAttribArray(2);
   glBindVertexArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);     // Note that this is allowed, the call to glVertexAttribPointer
                                        // registered VBO as the currently bound vertex buffer object so
                                        // afterwards we can safely unbind
  // clang-format on

  glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any
                        // buffer/array to prevent strange bugs)

  // Load texture
  int width, height;
  // clang-format off
    unsigned char *image = SOIL_load_image("container.jpg", &width, &height, nullptr, SOIL_LOAD_RGB);

    GLuint texture;
    glGenTextures(1, &texture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);

     glTexImage2D(GL_TEXTURE_2D,       // сообщили функции, что наша текстура привязана к этой цели (чтобы другие цели GL_TEXTURE_1D и GL_TEXTURE_3D не будут задействованы).
                  0,                   // аргумент описывает уровень мипмапа для которого мы хотим сгенерировать текстуру
                  GL_RGB,              // в каком формате мы хотим хранить текстуру
                  width, height,       // аргументы задают ширину и высоту результирующей текстуры, получили эти значения ранее во время загрузки изображения
                  0,                   // всегда должен быть 0. (Аргумент устарел).
                  GL_RGB,              // формат исходного изображения
                  GL_UNSIGNED_BYTE,    // тип данных исходного изображения
                  image                // данные изображения.
                  );
     glGenerateMipmap(GL_TEXTURE_2D);
  // clang-format on

  SOIL_free_image_data(image);

  // clang-format off
      image = SOIL_load_image("Gothic-II-3-icon.png", &width, &height, nullptr, SOIL_LOAD_RGB);
      GLuint texture1;
      glGenTextures(1, &texture1);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, texture1);

      glTexImage2D(GL_TEXTURE_2D,       // сообщили функции, что наша текстура привязана к этой цели (чтобы другие цели GL_TEXTURE_1D и GL_TEXTURE_3D не будут задействованы).
                   0,                   // аргумент описывает уровень мипмапа для которого мы хотим сгенерировать текстуру
                   GL_RGB,              // в каком формате мы хотим хранить текстуру
                   width, height,       // аргументы задают ширину и высоту результирующей текстуры, получили эти значения ранее во время загрузки изображения
                   0,                   // всегда должен быть 0. (Аргумент устарел).
                   GL_RGB,              // формат исходного изображения
                   GL_UNSIGNED_BYTE,    // тип данных исходного изображения
                   image                // данные изображения.
                   );
       glGenerateMipmap(GL_TEXTURE_2D);
  // clang-format on
  SOIL_free_image_data(image);

  bool run = true;
  SDL_Event e;

  while (run) {
    while (SDL_PollEvent(&e) != 0) {
      if (e.type == SDL_QUIT) {
        run = false;
      }
    }

    glClearColor(0.2f, .3f, .3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    simpleShader.Use();
    // Bind Textures using texture units
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture1);
    glUniform1i(glGetUniformLocation(simpleShader.Program, "ourTexture1"), 0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(glGetUniformLocation(simpleShader.Program, "ourTexture2"), 1);

    // Draw our first triangle

    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

    // Draw debug lines
    simpleDebugShader.Use();
    glBindVertexArray(VAO);
    glDrawElements(GL_LINE_LOOP, 6, GL_UNSIGNED_INT, nullptr);
    glBindVertexArray(0);
    SDL_GL_SwapWindow(window);
    // SDL_Delay(40);
  }

  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
};
