#pragma once

#include "SOIL/SOIL.h"
#include "glad/glad.h" // OpenGL ES 3.0
#include <fstream>
#include <string>
#include <vector>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

class Game_object {
public:
  Game_object();
  int load_vertices(const char *path);
  std::vector<GLfloat> vertices;
  std::vector<GLfloat> indices;
  int width, height;
  unsigned char *sprite = nullptr;
  GLuint texture;
  glm::mat4 model;
  glm::mat4 view;
  glm::mat4 projection;
};
