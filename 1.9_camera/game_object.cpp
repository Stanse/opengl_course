#include "game_object.hpp"
#include <algorithm>
#include <cstdlib> // для использования exit()
#include <fstream>
#include <iostream>
#include <string>

Game_object::Game_object()
{
    vertices = {
        // Positions          // Texture Coords
        1.0f,  1.0f,  0.0f, 0.6f, 1.0f, // Top Right
        1.0f,  -1.0f, 0.0f, 0.6f, 0.5f, // Bottom Right
        -1.0f, -1.0f, 0.0f, 0.4f, 0.5f, // Bottom Left

        -1.0f, 1.0f,  0.0f, 0.4f, 1.0f, // Top Left
        1.0f,  1.0f,  0.0f, 0.6f, 1.0f, // Top Right
        -1.0f, -1.0f, 0.0f, 0.4f, 0.5f, // Bottom Left
    };
}

int Game_object::load_vertices(const char* path)
{
    using namespace std;
    ifstream file;
    vertices.clear();
    GLfloat v;
    file.open(path, ios::binary | ios::in);
    if (!file)
    {
        cerr << "error: can't open " << path << endl;
        exit(1);
    }
    while (!file.eof())
    {
        file >> v;
        if (!file.eof())
            vertices.push_back(v);
    }
    file.close();
    return 0;
};
