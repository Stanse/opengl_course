#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "glad/glad.h" // OpenGL ES 3.0

class Shader {
public:
  Shader(const GLchar *vertexPath, const GLchar *fragmentPath);
  GLuint Program;
  void Use();
};
