#include "engine.hpp"
#include <iostream>

#include <cstdlib> // для использования exit()
#include <fstream>
#include <string>

using namespace std;

extern GLfloat lastFrame;
extern GLfloat deltaTime;

int main()
{
    Engine engine(800, 600);

    engine.init();
    //  Game_object bg;
    //  engine.bind_game_object(bg);
    //  engine.load_texture(bg, "container.jpg");
    Game_object player;
    player.load_vertices("vert.txt");
    engine.bind_game_object(player);
    // Load and create a texture
    engine.load_texture(player, "container.jpg");

    // ====================
    // GAME LOOP
    // ====================
    bool run = true;

    while (run)
    {
        engine.read_input(run);
        engine.do_movement(run);
        glClearColor(0.2f, .3f, .3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // engine.clear_display();

        engine.draw(player);

        GLfloat currentFrame = engine.getime();
        deltaTime            = currentFrame - lastFrame;
        lastFrame            = currentFrame;
    }

    engine.quit();

    return 0;
}
