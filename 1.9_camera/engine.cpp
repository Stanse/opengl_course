#include "engine.hpp"
#include "cassert"
#include <algorithm>
#include <iostream>

GLfloat deltaTime = 1.0f; // Время, прошедшее между последним
                          // и текущим кадром
GLfloat lastFrame = 0.0f; // Время вывода последнего кадра

Engine::Engine(int w, int h)
{
    screen_width  = w;
    screen_height = h;

    lastX = screen_width / 2;
    lastY = screen_height / 2;
    yaw   = -90.0f;
    pitch = 0.0f;
    fov   = 45.0f;

    cameraPos   = glm::vec3(0.0f, 0.0f, 3.0f);
    cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
    cameraUp    = glm::vec3(0.0f, 1.0f, 0.0f);
};

int Engine::init()
{
    using namespace std;
    stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };
    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);
    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
    {
        cerr << "warning: SDL2 compiled and linked version mismatch: "
             << &compiled << " " << &linked << endl;
        return 1;
    }

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        cerr << "error: can't init: " << SDL_GetError() << endl;
        system("pause");
        return 1;
    }

    window = SDL_CreateWindow("Lost viking", SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, screen_width,
                              screen_height, ::SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        cerr << "error: can't create window: " << SDL_GetError() << std::endl;
        system("pause");
        return 1;
    }

    int gl_major_ver = 3;
    int gl_minor_ver = 0;

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_CORE);
        gl_context = SDL_GL_CreateContext(window);
    }

    assert(gl_context != nullptr);

    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    assert(result == 0);

    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    assert(result == 0);

    if (gl_major_ver < 3)
    {
        clog << "current context opengl version: " << gl_major_ver << '.'
             << gl_minor_ver << '\n'
             << "need openg version at least: 3.0\n"
             << flush;
        throw runtime_error("opengl version too low");
    }

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0)
    {
        cerr << "error: failed to initialize glad" << endl;
    }
    // ********* Build and compile our shader program ********* //
    shader_prog = new Shader("shaders/shader.vs", "shaders/shader.frag");
    print_opengl_version();

    // ====================
    // Setup OpenGL options
    // ====================
    glEnable(GL_DEPTH_TEST);

    // SDL_ShowCursor(SDL_FALSE); // HIDE CURSOR
    return 0;
};

void Engine::swap_buffers()
{
    SDL_GL_SwapWindow(window);
    validate_opengl_errors();
}

void Engine::bind_game_object(Game_object& game_obj)
{
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER,
                 static_cast<long>(game_obj.vertices.size()) *
                     static_cast<long>(sizeof(GLfloat)),
                 &game_obj.vertices.front(), GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
                          static_cast<GLvoid*>(nullptr));
    glEnableVertexAttribArray(0);
    // TexCoord attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
                          (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(0); // Unbind VAO
};

void Engine::load_texture(Game_object& game_obj, const char* path)
{
    glGenTextures(1, &game_obj.texture);
    glBindTexture(GL_TEXTURE_2D,
                  game_obj.texture); // All upcoming GL_TEXTURE_2D operations
                                     // now have effect on our texture object
    // Set our texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                    GL_REPEAT); // Set texture wrapping to GL_REPEAT
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Set texture filtering
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Load, create texture and generate mipmaps
    // clang-format off
    game_obj.sprite = SOIL_load_image(path,                   // путь к файлу
                                      &game_obj.width,        // указатели в которые будут
                                      &game_obj.height,       // помещены  размеры изображения
                                      nullptr,                // количество каналов изображения
                                      SOIL_LOAD_RGB);         // как загружать изображение

    glTexImage2D(GL_TEXTURE_2D,             // текстурная цель
                 0,                         // уровень мипмапа для которого мы хотим сгенерировать текстуру
                 GL_RGB,                    // формат хранения текстуры
                 game_obj.width,            // ширина и
                 game_obj.height,           // высота результирующей текстуры
                 0,                         // всегда должен быть 0. (Аргумент устарел).
                 GL_RGB,                    // формат исходного изображения
                 GL_UNSIGNED_BYTE,          // тип данных исходного изображения
                 game_obj.sprite            // данные
                 );
    // clang-format on
    if (game_obj.sprite == nullptr)
    {
        std::cerr << "error: can't open " << path << std::endl;
    }
    glGenerateMipmap(GL_TEXTURE_2D);
    SOIL_free_image_data(game_obj.sprite);
    glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't
                                     // accidentily mess up our texture.
};

void Engine::draw(Game_object& game_obj)
{
    // Activate shader
    shader_prog->Use();

    // Camera/View transformation
    GLfloat timeInSec = SDL_GetTicks() * 0.001f;

    glm::mat4 view;
    //    GLfloat   radius = 5.0f;
    //    GLfloat   camX   = sin(timeInSec) * radius;
    //    GLfloat   camZ   = cos(timeInSec) * radius;
    //    view = glm::lookAt(glm::vec3(camX, 0.0f, camZ), glm::vec3(0.0f, 0.0f,
    //    0.0f),
    //                       glm::vec3(0.0f, 1.0f, 0.0f));
    // view  = glm::translate(view, glm::vec3(0.0f, 0.0f, -5.0f));
    view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);

    // Model
    glm::mat4 model;
    GLfloat   angle = timeInSec * 50.0f;
    model           = glm::rotate(model, 0.0f, glm::vec3(1.0f, 1.0f, 0.0f));
    model           = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));

    // Projection
    glm::mat4 projection;
    // projection = glm::ortho(0.0f, 800.0f, 600.0f, 0.0f, 0.1f, 100.0f);
    projection = glm::perspective(
        fov, screen_width / static_cast<float>(screen_height), 0.1f, 10.0f);

    // Bind Textures using texture units
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, game_obj.texture);
    glUniform1i(glGetUniformLocation(shader_prog->Program, "ourTexture1"), 0);

    // Get matrix's uniform location and set matrix
    // clang-format off
      GLint modelLoc = glGetUniformLocation(shader_prog->Program, "model");
      glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

      GLint viewLoc = glGetUniformLocation(shader_prog->Program, "view");
      glad_glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

      GLint projectionLoc = glGetUniformLocation(shader_prog->Program, "projection");
      glUniformMatrix4fv(projectionLoc,                 // позиция переменной
                         1,                             // кол-во матриц
                         GL_FALSE,                      // требуется транспонировать матрицу?
                         glm::value_ptr(projection)     // data
                         );
    // clang-format on

    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    SDL_GL_SwapWindow(window);
};

int Engine::quit()
{
    SDL_DestroyWindow(window);
    SDL_Quit();
    // delete shader_prog;
    return 0;
};

void Engine::clear_display()
{
    glClearColor(.0f, .0f, .0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT /*| GL_DEPTH_BUFFER_BIT*/);
};

Engine::~Engine()
{
    this->quit();
};

void Engine::validate_opengl_errors()
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        std::string message;
        switch (error)
        {
            case GL_INVALID_ENUM:
                message =
                    "invalid enum passed to GL function (GL_INVALID_ENUM)";
                break;
            case GL_INVALID_VALUE:
                message = "invalid parameter passed to GL function "
                          "(GL_INVALID_VALUE)";
                break;
            case GL_INVALID_OPERATION:
                message = "cannot execute some of GL functions in current "
                          "state (GL_INVALID_OPERATION)";
                break;
            case GL_OUT_OF_MEMORY:
                message = "no enough memory to execute GL function "
                          "(GL_OUT_OF_MEMORY)";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                message = "invalid framebuffer operation "
                          "(GL_INVALID_FRAMEBUFFER_OPERATION)";
                break;
            default:
                message =
                    "error in some GL extension (framebuffers, shaders, etc)";
                break;
        }
        std::cerr << "OpenGL error: " << message << std::endl;
        std::abort();
    }
};

void Engine::print_opengl_version()
{
    using namespace std;
    string version     = reinterpret_cast<const char*>(glGetString(GL_VERSION));
    string vendor_info = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
    string extentions_info =
        reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
    clog << "OpenGL version: " << version << endl;
    clog << "OpenGL vendor: " << vendor_info << endl;
    // cerr << "Full OpenGL extention list: " << extentions_info << endl;
};

// CAMERA
void Engine::read_input(bool& run)
{
    while (SDL_PollEvent(&e) != 0)
    {
        if (e.type == SDL_QUIT)
        {
            run = false;
        }

        // KEYDOWN
        if (e.type == SDL_KEYDOWN)
        {
            keys[e.key.keysym.sym] = true;
        }
        // KEYUP
        if (e.type == SDL_KEYUP)
        {
            keys[e.key.keysym.sym] = false;
        }

        // MOUSE WHEEL
        if (e.type == SDL_MOUSEWHEEL)
        {
            GLfloat offset = 1;
            if (e.wheel.y > 0) // scroll up
            {
                if (fov < 45)
                {
                    fov += offset;
                }
                else
                {
                    fov = 45;
                }
            }
            else if (e.wheel.y < 0) // scroll down
            {
                if (fov > 1)
                {
                    fov -= offset;
                }
                else
                {
                    fov = 1;
                }
            }
        }

        // MOUSE MOUTION
        if (e.type == SDL_MOUSEMOTION)
        {
            SDL_GetMouseState(&xpos, &ypos);
            if (first_mouse)
            {
                lastX       = xpos;
                lastY       = ypos;
                first_mouse = false;
            }

            GLfloat xoffset = xpos - lastX;
            // Reversed since y-coordinates go from bottom to left
            GLfloat yoffset = lastY - ypos;
            lastX           = xpos;
            lastY           = ypos;

            GLfloat sensitivity = 0.05; // Change this value to your liking
            xoffset *= sensitivity;
            yoffset *= sensitivity;

            yaw += xoffset * 4;
            pitch += yoffset * 4;

            // Make sure that when pitch is out of bounds, screen doesn't get
            // flipped
            if (pitch > 89.0f)
                pitch = 89.0f;
            if (pitch < -89.0f)
                pitch = -89.0f;

            glm::vec3 front;
            front.x     = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
            front.y     = sin(glm::radians(pitch));
            front.z     = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
            cameraFront = glm::normalize(front);
        }
    }
}

void Engine::do_movement(bool& run)
{
    cameraSpeed = 0.005f * deltaTime;
    if (keys[SDLK_ESCAPE])
        run = false;
    if (keys[SDLK_w])
    {
        cameraPos += cameraSpeed * cameraFront;
    }
    if (keys[SDLK_s])
    {
        cameraPos -= cameraSpeed * cameraFront;
    }
    if (keys[SDLK_a])
    {
        cameraPos -=
            glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    }
    if (keys[SDLK_d])
    {
        cameraPos +=
            glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    }
}
