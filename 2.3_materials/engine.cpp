#include "engine.hpp"
#include "cassert"
#include "stb_image.h"
#include <algorithm>
#include <iostream>

GLfloat deltaTime = 0.0f; // Время, прошедшее между последним
                          // и текущим кадром
GLfloat lastFrame = 0.0f; // Время вывода последнего кадра

Engine::Engine(int w, int h)
{
    screen_width  = w;
    screen_height = h;

    // camera
    lastX        = screen_width / 2;
    lastY        = screen_height / 2;
    camera       = (glm::vec3(7.5f, 15.0f, -0.5f));
    camera.yaw   = -180.0f;
    camera.pitch = -61.0f;
    camera.update_camera_vectors();

    // lighting
    lightPos = glm::vec3(1.2f, 1.2f, 2.0f);
};

int Engine::init()
{
    using namespace std;
    stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };
    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);
    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
    {
        cerr << "warning: SDL2 compiled and linked version mismatch: "
             << &compiled << " " << &linked << endl;
        return 1;
    }

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        cerr << "error: can't init: " << SDL_GetError() << endl;
        system("pause");
        return 1;
    }

    window = SDL_CreateWindow("Lost viking", SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, screen_width,
                              screen_height, ::SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        cerr << "error: can't create window: " << SDL_GetError() << std::endl;
        system("pause");
        return 1;
    }

    int gl_major_ver = 3;
    int gl_minor_ver = 0;

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_CORE);
        gl_context = SDL_GL_CreateContext(window);
    }

    assert(gl_context != nullptr);

    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    assert(result == 0);

    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    assert(result == 0);

    if (gl_major_ver < 3)
    {
        clog << "current context opengl version: " << gl_major_ver << '.'
             << gl_minor_ver << '\n'
             << "need openg version at least: 3.0\n"
             << flush;
        throw runtime_error("opengl version too low");
    }

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0)
    {
        cerr << "error: failed to initialize glad" << endl;
    }
    // ********* Build and compile our shader program ********* //
    lightingShader = new Shader("shaders/material.vs", "shaders/material.fs");
    lampShader     = new Shader("shaders/lamp.vs", "shaders/lamp.fs");
    print_opengl_version();

    // ====================
    // Setup OpenGL options
    // ====================
    glEnable(GL_DEPTH_TEST);
    // SDL_ShowCursor(SDL_FALSE); // HIDE CURSOR

    // VAO and VBO
    glGenVertexArrays(1, &light_VAO);
    glGenBuffers(1, &light_VBO);

    glGenVertexArrays(1, &game_obj_VAO);
    glGenBuffers(1, &game_obj_VBO);

    validate_opengl_errors();
    return 0;
};

void Engine::swap_buffers()
{
    SDL_GL_SwapWindow(window);
    validate_opengl_errors();
}

void Engine::bind_game_object(Game_object& game_obj)
{

    glBindVertexArray(game_obj_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, game_obj_VBO);
    glBufferData(GL_ARRAY_BUFFER,
                 static_cast<long>(game_obj.vertices.size()) *
                     static_cast<long>(sizeof(GLfloat)),
                 &game_obj.vertices.front(), GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
                          static_cast<GLvoid*>(nullptr));
    glEnableVertexAttribArray(0);
    // Normal attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
                          reinterpret_cast<GLvoid*>(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(0); // game_obj_VAO

    // light's VAO and VBO
    glBindVertexArray(light_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, light_VBO);
    glBufferData(GL_ARRAY_BUFFER,
                 static_cast<long>(game_obj.vertices.size()) *
                     static_cast<long>(sizeof(GLfloat)),
                 &game_obj.vertices.front(), GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
                          static_cast<GLvoid*>(nullptr));
    glEnableVertexAttribArray(0);
    // Normal attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
                          reinterpret_cast<GLvoid*>(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(0); // Unbind light_VAO
};

void Engine::load_texture(Game_object& game_obj, const char* path)
{
    glGenTextures(1, &game_obj.texture);
    glBindTexture(GL_TEXTURE_2D,
                  game_obj.texture); // All upcoming GL_TEXTURE_2D operations
                                     // now have effect on our texture object
    // Set our texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                    GL_REPEAT); // Set texture wrapping to GL_REPEAT
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Set texture filtering
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Load, create texture and generate mipmaps
    // clang-format off
//    game_obj.sprite = SOIL_load_image(path,                   // путь к файлу
//                                      &game_obj.width,        // указатели в которые будут
//                                      &game_obj.height,       // помещены  размеры изображения
//                                      nullptr,                // количество каналов изображения
//                                      SOIL_LOAD_RGB);         // как загружать изображение

    glTexImage2D(GL_TEXTURE_2D,             // текстурная цель
                 0,                         // уровень мипмапа для которого мы хотим сгенерировать текстуру
                 GL_RGB,                    // формат хранения текстуры
                 game_obj.width,            // ширина и
                 game_obj.height,           // высота результирующей текстуры
                 0,                         // всегда должен быть 0. (Аргумент устарел).
                 GL_RGB,                    // формат исходного изображения
                 GL_UNSIGNED_BYTE,          // тип данных исходного изображения
                 game_obj.sprite            // данные
                 );
    // clang-format on
    if (game_obj.sprite == nullptr)
    {
        std::cerr << "error: can't open " << path << std::endl;
    }
    glGenerateMipmap(GL_TEXTURE_2D);
    // SOIL_free_image_data(game_obj.sprite);
    glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't
                                     // accidentily mess up our texture.
};

void Engine::draw(Game_object& game_obj)
{
    // be sure to activate shader when setting uniforms/drawing objects
    lightingShader->use();
    lightingShader->setVec3("light.position", lightPos);
    lightingShader->setVec3("viewPos", camera.position);

    // light properties
    glm::vec3 lightColor;
    lightColor.x = sin(getime() * 0.001f * 2.0f) / 2.0f + 0.5f;
    lightColor.y = sin(getime() * 0.001f * 0.7f) / 2.0f + 0.5f;
    lightColor.z = sin(getime() * 0.001f * 1.3f) / 2.0f + 0.5f;
    glm::vec3 diffuseColor =
        lightColor * glm::vec3(0.5f); // decrease the influence
    glm::vec3 ambientColor = diffuseColor * glm::vec3(0.2f); // low influence
    lightingShader->setVec3("light.ambient", ambientColor);
    lightingShader->setVec3("light.diffuse", diffuseColor);
    lightingShader->setVec3("light.specular", 1.0f, 1.0f, 1.0f);

    // material properties
    lightingShader->setVec3("material.ambient", 1.0f, 0.5f, 0.31f);
    lightingShader->setVec3("material.diffuse", 1.0f, 0.5f, 0.31f);
    lightingShader->setVec3("material.specular", 0.5f, 0.5f,
                            0.5f); // specular lighting doesn't have full effect
                                   // on this object's material
    lightingShader->setFloat("material.shininess", 32.0f);

    // view/projection transformations
    glm::mat4 view       = camera.GetViewMatrix();
    glm::mat4 projection = glm::perspective(
        camera.zoom, screen_width / static_cast<float>(screen_height), 0.1f,
        100.0f);

    lightingShader->setMat4("projection", projection);
    lightingShader->setMat4("view", view);

    // world transformation
    glm::mat4 model = glm::mat4(1.0f);
    model           = glm::scale(model, glm::vec3(10.0f, 1.0f, 10.0f));
    lightingShader->setMat4("model", model);

    // Draw the container (using container's vertex attributes)
    glBindVertexArray(game_obj_VAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    // Also draw the lamp object, again binding the appropriate shader
    lampShader->use();
    lampShader->setMat4("projection", projection);
    lampShader->setMat4("view", view);

    model      = glm::mat4(1.0f);
    model      = glm::translate(model, lightPos);
    lightPos.x = 1.0f + sin(getime() * 0.001f) * 2.0f;
    lightPos.z = sin(0.001f * getime() / 2.0f) * 1.0f;

    model = glm::scale(model, glm::vec3(0.8f)); // Make it a smaller cube
    lampShader->setMat4("model", model);

    // Draw the light object (using light's vertex attributes)
    glBindVertexArray(light_VAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    // Swap the screen buffers
    SDL_GL_SwapWindow(window);
};

int Engine::quit()
{
    SDL_DestroyWindow(window);
    SDL_Quit();
    //    if (lightingShader != nullptr)
    //        delete lightingShader;
    //    if (lampShader != nullptr)
    //        delete lampShader;
    return 0;
};

void Engine::clear_display()
{
    glClearColor(.0f, .0f, .0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);
};

Engine::~Engine()
{
    this->quit();
};

void Engine::validate_opengl_errors()
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        std::string message;
        switch (error)
        {
            case GL_INVALID_ENUM:
                message =
                    "invalid enum passed to GL function (GL_INVALID_ENUM)";
                break;
            case GL_INVALID_VALUE:
                message = "invalid parameter passed to GL function "
                          "(GL_INVALID_VALUE)";
                break;
            case GL_INVALID_OPERATION:
                message = "cannot execute some of GL functions in current "
                          "state (GL_INVALID_OPERATION)";
                break;
            case GL_OUT_OF_MEMORY:
                message = "no enough memory to execute GL function "
                          "(GL_OUT_OF_MEMORY)";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                message = "invalid framebuffer operation "
                          "(GL_INVALID_FRAMEBUFFER_OPERATION)";
                break;
            default:
                message =
                    "error in some GL extension (framebuffers, shaders, etc)";
                break;
        }
        std::cerr << "OpenGL error: " << message << std::endl;
        std::abort();
    }
};

void Engine::print_opengl_version()
{
    using namespace std;
    string version     = reinterpret_cast<const char*>(glGetString(GL_VERSION));
    string vendor_info = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
    string extentions_info =
        reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
    clog << "OpenGL version: " << version << endl;
    clog << "OpenGL vendor: " << vendor_info << endl;
    // cerr << "Full OpenGL extention list: " << extentions_info << endl;
};

GLfloat Engine::calc_delta_time()
{
    GLfloat currentFrame = getime();
    deltaTime            = currentFrame - lastFrame;
    lastFrame            = currentFrame;
    return deltaTime;
}

// CAMERA
void Engine::read_input(bool& run)
{
    while (SDL_PollEvent(&e) != 0)
    {
        if (e.type == SDL_QUIT)
        {
            run = false;
        }

        // KEYDOWN
        if (e.type == SDL_KEYDOWN)
        {
            keys[e.key.keysym.sym] = true;
        }
        // KEYUP
        if (e.type == SDL_KEYUP)
        {
            keys[e.key.keysym.sym] = false;
        }

        // MOUSE WHEEL
        if (e.type == SDL_MOUSEWHEEL)
        {
            GLfloat offset = 1;
            if (e.wheel.y > 0) // scroll up
            {
                if (camera.zoom < 45)
                {
                    camera.zoom += offset;
                }
                else
                {
                    camera.zoom = 45;
                }
            }
            else if (e.wheel.y < 0) // scroll down
            {
                if (camera.zoom > 1)
                {
                    camera.zoom -= offset;
                }
                else
                {
                    camera.zoom = 1;
                }
            }
        }

        // MOUSE MOUTION
        if (e.type == SDL_MOUSEMOTION)
        {
            SDL_GetMouseState(&xpos, &ypos);
            if (first_mouse)
            {
                lastX       = xpos;
                lastY       = ypos;
                first_mouse = false;
            }

            GLfloat xoffset = xpos - lastX;
            GLfloat yoffset =
                lastY -
                ypos; // Reversed since y-coordinates go from bottom to left
            lastX = xpos;
            lastY = ypos;

            xoffset *= camera.mouse_sensitivity;
            yoffset *= camera.mouse_sensitivity;

            camera.yaw += xoffset;
            camera.pitch += yoffset;

            // Make sure that when pitch is out of bounds, screen doesn't get
            // flipped
            if (camera.pitch > 89.0f)
                camera.pitch = 89.0f;
            if (camera.pitch < -89.0f)
                camera.pitch = -89.0f;

            camera.update_camera_vectors();
        }
    }
}

void Engine::do_movement(bool& run)
{
    float velocity = camera.movement_speed * deltaTime;
    if (keys[SDLK_ESCAPE])
        run = false;
    if (keys[SDLK_w])
    {
        camera.position += camera.front * velocity;
    }
    if (keys[SDLK_s])
    {
        camera.position -= camera.front * velocity;
    }
    if (keys[SDLK_a])
    {
        camera.position -= camera.right * velocity;
    }
    if (keys[SDLK_d])
    {
        camera.position += camera.right * velocity;
    }
}
