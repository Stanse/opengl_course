#version 330 core
precision mediump float;

//in vec3 ourColor;
in vec2 TexCoord;

out vec4 color;
vec4 tempColor;

uniform sampler2D ourTexture1;
uniform sampler2D ourTexture2;
uniform int showDebug = 0;

void main()
{
    tempColor = mix(texture2D(ourTexture1, TexCoord), texture2D(ourTexture2, TexCoord), 0.5);
    if(showDebug == 0) {
    color = tempColor;
   } else if(showDebug == 1){
        color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    }
}
