//#pragma once
#include <exception>
#include <functional>
#include <iostream>
#include <vector>

#include <glad/glad.h> // OpenGL ES 3.0

#include "SOIL/SOIL.h"
#include <SDL.h>
#include <SDL_opengles2.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

// Shaders
static const GLchar *vertexShaderSource = R"(
                                          #version 330 core
                                          layout (location = 0) in vec3 position;
                                          layout (location = 1) in vec3 color;
                                          layout (location = 2) in vec2 texCoord;

                                          out vec3 ourColor;
                                          out vec2 TexCoord;

                                          void main()
                                          {
                                              gl_Position = vec4(position, 1.0f);
                                              ourColor = color;
                                              TexCoord = texCoord;
                                          }
                                   )";

static const GLchar *fragmentShaderSource = R"(
                                            #version 330 core
                                            in vec3 ourColor;
                                            in vec2 TexCoord;

                                            out vec4 color;

                                            uniform sampler2D ourTexture;

                                            void main()
                                            {
                                                color = texture(ourTexture, TexCoord);
                                            }
                                     )";

static const GLchar *fragmentDebugShaderSource = R"(
                                            #version 330 core
                                            //out vec4 color;

                                            void main()
                                            {
                                                color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
                                            }
                                     )";

int main(int /*argc*/, char * /*args*/[]) {

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    return 1;
  }

  SDL_Surface *screen_surface = nullptr;
  SDL_Window *window = nullptr;

  window =
      SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       SCREEN_WIDTH, SCREEN_HEIGHT, ::SDL_WINDOW_OPENGL);

  if (window == nullptr) {
    return 1;
  }

  SDL_GLContext gl_context = SDL_GL_CreateContext(window);

  if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) { // Important!
    std::cout << "error: failed to initialize glad" << std::endl;
  }

  // Build and compile our shader program

  // Vertex shader
  GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexShaderSource, nullptr);
  glCompileShader(vertexShader);
  // Check for compile time errors
  {
    /*GLint success;
    GLchar infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog <<
    std::endl;
    */
  }

  // Fragment shader
  GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentShaderSource, nullptr);
  glCompileShader(fragmentShader);
  // Check for compile time errors
  {
    /*glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
      {
      glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
      std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog <<
    std::endl;
      }
      */
  }

  // Link shaders
  GLuint shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glLinkProgram(shaderProgram);
  // Check for linking errors
  {
    /*    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
        if (!success) {
            glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
            std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog
       << std::endl;
        }*/
  }

  // Fragment Debug shader
  GLuint fragmentDebugShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentDebugShaderSource, nullptr);
  glCompileShader(fragmentShader);

  // Link shaders
  GLuint shaderDebugProgram = glCreateProgram();
  glAttachShader(shaderDebugProgram, vertexShader);
  glAttachShader(shaderDebugProgram, fragmentDebugShader);
  glLinkProgram(shaderDebugProgram);

  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);

  // clang-format off
  GLfloat vertices[] = {
      // Позиции          // Цвета             // Текстурные координаты
       0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // Верхний правый
       0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // Нижний правый
      -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // Нижний левый
      -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // Верхний левый
  };
  GLfloat vertices_edge[] = {
      0.5f,  0.5f, 0.0f,  // Left
      0.5f, -0.5f, 0.0f,  // Right
      -0.5f, -0.5f, 0.0f,   // Top
      };
  // clang-format on
  GLuint indices[] = {
      // Note that we start from 0!
      0, 1, 3, // First Triangle
      1, 2, 3  // Second Triangle
  };

  GLuint VBOs[2], VAOs[2], EBO;
  // glad_glEnableVertexAttribArray(1);
  glGenVertexArrays(1, &VAOs[0]);
  glGenBuffers(1, &VBOs[0]);
  glGenBuffers(1, &EBO);

  // Bind the Vertex Array Object first, then bind and set vertex buffer(s) and
  // attribute pointer(s).
  glBindVertexArray(VAOs[0]);

  glBindBuffer(GL_ARRAY_BUFFER, VBOs[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
               GL_STATIC_DRAW);

  // clang-format off
  // Position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), static_cast<GLvoid*>(nullptr));
  glEnableVertexAttribArray(0);

  // Color attribute
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(3 * sizeof (GLfloat)));
  glEnableVertexAttribArray(1);

  // Texture coord attribute
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof (GLfloat), reinterpret_cast<GLvoid*>(6 * sizeof (GLfloat)));
  glEnableVertexAttribArray(2);
  glBindVertexArray(0);

  // Debug draw
  glBindVertexArray(VAOs[1]);	// Note that we bind to a different VAO now
  glBindBuffer(GL_ARRAY_BUFFER, VBOs[1]);	// And a different VBO
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_edge), vertices_edge, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof (GLfloat), static_cast<GLvoid*>(nullptr)); // Because the vertex data is tightly packed we can also specify 0 as the vertex attribute's stride to let OpenGL figure it out.
  glEnableVertexAttribArray(0);
  glBindVertexArray(0);


  glBindBuffer(GL_ARRAY_BUFFER, 0);     // Note that this is allowed, the call to glVertexAttribPointer
                                        // registered VBO as the currently bound vertex buffer object so
                                        // afterwards we can safely unbind

  // clang-format on
  glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any
                        // buffer/array to prevent strange bugs)

  // Load texture
  int width, height;
  unsigned char *image =
      SOIL_load_image("container.jpg", &width, &height, nullptr, SOIL_LOAD_RGB);
  // clang-format off
  GLuint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);

   glTexImage2D(GL_TEXTURE_2D,       // мы сообщили функции, что наша текстура привязана к этой цели (чтобы другие цели GL_TEXTURE_1D и GL_TEXTURE_3D не будут задействованы).
                0,                   // аргумент описывает уровень мипмапа для которого мы хотим сгенерировать текстуру
                GL_RGB,              // в каком формате мы хотим хранить текстуру
                width, height,       // аргументы задают ширину и высоту результирующей текстуры, получили эти значения ранее во время загрузки изображения
                0,                   // всегда должен быть 0. (Аргумент устарел).
                GL_RGB,              // формат исходного изображения
                GL_UNSIGNED_BYTE,    // тип данных исходного изображения
                image                // данные изображения.
                );
   glGenerateMipmap(GL_TEXTURE_2D);
  // clang-format on

  SOIL_free_image_data(image);

  bool run = true;
  SDL_Event e;

  while (run) {

    while (SDL_PollEvent(&e) != 0) {
      if (e.type == SDL_QUIT) {
        run = false;
      }
    }

    glClearColor(0.2f, .3f, .3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Bind Texture
    glBindTexture(GL_TEXTURE_2D, texture);

    // Draw our first triangle
    //    glUseProgram(shaderProgram);
    //    glBindVertexArray(VAOs[0]);
    //    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

    glUseProgram(shaderDebugProgram);
    glBindVertexArray(VAOs[1]);
    glDrawArrays(GL_LINE_LOOP, 0, 3);
    glBindVertexArray(0);
    SDL_GL_SwapWindow(window);
    SDL_Delay(40);
  }

  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
};
