cmake_minimum_required(VERSION 3.16)

project(hello_triangle_IBO CXX C)

file(GLOB
    SOIL_headers "SOIL/*.h")
file(GLOB
    SOIL_c "SOIL/*.c")

add_executable(hello_triangle_IBO
    main.cpp
    glad/glad.c
    ${SOIL_c}
    )
target_compile_features(hello_triangle_IBO PUBLIC cxx_std_17)


find_package(sdl2 REQUIRED)

target_include_directories(hello_triangle_IBO PUBLIC
    ${SDL2_INCLUDE_DIRS}
    ${SOIL_headers}
    ${CMAKE_CURRENT_LIST_DIR}
    /SOIL
    )
target_link_libraries(hello_triangle_IBO PUBLIC
    ${SDL2_LIBRARIES}
    #/usr/local/lib/libSDL2-2.0d.so
    )

target_link_libraries(hello_triangle_IBO PRIVATE
    -lSDL2
    -lGL
    /home/stanse/c++/gameDev/openGL/OpenGL_course/hello_triangle_IBO/lib/libSOIL.a
    )
