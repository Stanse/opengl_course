cmake_minimum_required(VERSION 3.16)

project(hello_triangle_2sh CXX C)

file(GLOB
    SOIL_headers "SOIL/*.h")
file(GLOB
    SOIL_c "SOIL/*.c")

add_executable(hello_triangle_2sh
    main.cpp
    glad/glad.c
    shader.cpp
    ${SOIL_c}
    )
target_compile_features(hello_triangle_2sh PUBLIC cxx_std_17)


find_package(sdl2 REQUIRED)

target_include_directories(hello_triangle_2sh PUBLIC
    ${SDL2_INCLUDE_DIRS}
    ${SOIL_headers}
    ${CMAKE_CURRENT_LIST_DIR}
    /SOIL
    /lib
    )
target_link_libraries(hello_triangle_2sh PUBLIC
    ${SDL2_LIBRARIES}
    #/usr/local/lib/libSDL2-2.0d.so
    )

target_link_libraries(hello_triangle_2sh PRIVATE
    -lSDL2
    -lGL
    /home/Stanse/Qt_projects/gameDev/openGL/OpenGL_course/hello_triangle_2sh/lib/libSOIL.a
    )
